//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.wshy.core.util;

import com.wshy.core.exception.ExceptionCenter;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.http.HttpStatus;

public class ParamUtil {
    public ParamUtil() {
    }

    public static String getDataSource(String driverClassName) {
        if (driverClassName.contains("oracle")) {
            return "oracle";
        } else {
            return driverClassName.contains("sqlserver") ? "sqlserver" : "mysql";
        }
    }

    public static Map<String, Object> addDataSource(Map<String, Object> paramMap, String datasource) {
        paramMap.put("datasource", getDataSource(datasource));
        return paramMap;
    }

    public static Map<String, Object> filterEmpty(Map<String, Object> paramMap) {
        Iterator it = paramMap.entrySet().iterator();

        while(true) {
            Entry item;
            do {
                if (!it.hasNext()) {
                    return paramMap;
                }

                item = (Entry)it.next();
            } while(item.getValue() != null && !"".equals(item.getValue()));

            it.remove();
        }
    }

    public static Map removeKeysOfMap(Map paramMap, String... keys) {
        Map param = new HashMap();
        param.putAll(paramMap);

        for(int i = 0; i < keys.length; ++i) {
            param.remove(keys[i]);
        }

        return param;
    }

    public static Map arrayToMap(Map param, Object... args) {
        if (param == null) {
            param = new HashMap();
        }

        for(int i = 0; i < args.length; i += 2) {
            ((Map)param).put(args[i], args[i + 1]);
        }

        return (Map)param;
    }

    public static void checkParam(Map paramMap, String key, String name) throws ExceptionCenter {
        if (paramMap.get(key) == null) {
            throw new ExceptionCenter(HttpStatus.INTERNAL_SERVER_ERROR, "缺少" + name);
        }
    }

    public static Map<String, Object> transformToListParam(Map<String, Object> paramMap) throws ExceptionCenter {
        String toList = (String)paramMap.get("toList");
        if (toList != null) {
            String[] array = toList.split(",");
            String[] var3 = array;
            int var4 = array.length;

            for(int var5 = 0; var5 < var4; ++var5) {
                String key = var3[var5];
                String toListType = (String)paramMap.get("toListType");
                List value;
                if ("int".equals(toListType)) {
                    value = (List)paramMap.get(key);
                    paramMap.put(key, value);
                } else if ("map".equals(toListType)) {
                    value = (List)paramMap.get(key);
                    paramMap.put(key, value);
                } else {
                    value = (List)paramMap.get(key);
                    paramMap.put(key, value);
                }
            }
        }

        return paramMap;
    }

    public static void convertParamToList(Map<String, Object> paramMap) {
        String toList = (String)paramMap.get("toList");
        if (toList != null) {
            String[] array = toList.split(",");
            String[] var3 = array;
            int var4 = array.length;

            for(int var5 = 0; var5 < var4; ++var5) {
                String key = var3[var5];
                String value = (String)paramMap.get(key);
                if (value != null) {
                    paramMap.put(key + "List", Arrays.asList(value.split(",")));
                }
            }
        }

    }

    public static String getError(Exception e) {
        if (e.getMessage() == null) {
            return "服务异常";
        } else if (e.getMessage().contains("Table 'geit_queue.queue_2019_08_25' doesn't exist") && e.getMessage().contains("doesn't exist")) {
            int start = e.getMessage().indexOf("Table");
            int end = e.getMessage().indexOf("doesn't exist");
            return e.getMessage().substring(start, end) + "不存在";
        } else if (e.getMessage().contains("Duplicate entry")) {
            return "添加了重复的记录";
        } else if (e.getMessage().contains("Data too long")) {
            return "参数长度过长";
        } else if (e.getMessage().contains("unique constraint")) {
            return "id字段重复，请检查";
        } else if (e.getMessage().contains("ORA-00904")) {
            return "无效列名";
        } else if (e.getMessage().contains("ORA-00942")) {
            return "表或者视图不存在";
        } else if (e.getMessage().contains("ORA-01400")) {
            return "不能将空值插入";
        } else if (e.getMessage().contains("ORA-00936")) {
            return "缺少表达式";
        } else if (e.getMessage().contains("ORA-00932")) {
            return "数据类型不一致";
        } else if (e.getMessage().contains("ORA-00933")) {
            return "SQL 命令未正确结束";
        } else if (e.getMessage().contains("ORA-01722")) {
            return "无效的数字赋值";
        } else if (e.getMessage().contains("ORA-06592")) {
            return "case语句格式有误";
        } else if (e.getMessage().contains("ORA-00001")) {
            return "主键唯一值冲突";
        } else if (e.getMessage().contains("ORA-00001")) {
            return "主键唯一值冲突，请检查id值";
        } else if (e.getMessage().contains("ORA-01422")) {
            return "返回超过一行";
        } else if (e.getMessage().contains("NullPointerException")) {
            return "空指针异常";
        } else if (e.getMessage().contains("ClassNotFoundException")) {
            return "类不存在";
        } else if (e.getMessage().contains("NumberFormatException")) {
            return "字符串转换为数字异常";
        } else if (e.getMessage().contains("IndexOutOfBoundsException")) {
            return "数组越界";
        } else if (e.getMessage().contains("IllegalArgumentException")) {
            return "方法的参数错误";
        } else {
            return e.getMessage().contains("NullPointerException") ? "空指针异常" : e.getMessage();
        }
    }

    public static String getErrorCode(Exception e) {
        if (e.getMessage() == null) {
            return "500";
        } else if (e.getMessage().contains("Table") && e.getMessage().contains("doesn't exist")) {
            int start = e.getMessage().indexOf("Table");
            int end = e.getMessage().indexOf("doesn't exist");
            return "501";
        } else if (e.getMessage().contains("Duplicate entry")) {
            return "502";
        } else if (e.getMessage().contains("Data too long")) {
            return "503";
        } else if (e.getMessage().contains("unique constraint")) {
            return "504";
        } else if (e.getMessage().contains("ORA-00904")) {
            return "无效列名";
        } else if (e.getMessage().contains("ORA-00942")) {
            return "表或者视图不存在";
        } else if (e.getMessage().contains("ORA-01400")) {
            return "不能将空值插入";
        } else if (e.getMessage().contains("ORA-00936")) {
            return "缺少表达式";
        } else if (e.getMessage().contains("ORA-00932")) {
            return "数据类型不一致";
        } else if (e.getMessage().contains("ORA-00933")) {
            return "SQL 命令未正确结束";
        } else if (e.getMessage().contains("ORA-01722")) {
            return "无效的数字赋值";
        } else if (e.getMessage().contains("ORA-06592")) {
            return "case语句格式有误";
        } else if (e.getMessage().contains("ORA-00001")) {
            return "主键唯一值冲突";
        } else if (e.getMessage().contains("ORA-00001")) {
            return "主键唯一值冲突，请检查id值";
        } else if (e.getMessage().contains("ORA-01422")) {
            return "返回超过一行";
        } else if (e.getMessage().contains("NullPointerException")) {
            return "505";
        } else if (e.getMessage().contains("ClassNotFoundException")) {
            return "506";
        } else if (e.getMessage().contains("NumberFormatException")) {
            return "507";
        } else if (e.getMessage().contains("IndexOutOfBoundsException")) {
            return "508";
        } else {
            return e.getMessage().contains("IllegalArgumentException") ? "509" : "599";
        }
    }

    public static String getMapValue(Map paramMap, String key, String defVal) {
        return paramMap.get(key) != null ? (String)paramMap.get(key) : defVal;
    }

    public static String formatParam(Map paramMap) {
        String sb = "";

        Entry item;
        for(Iterator it = paramMap.entrySet().iterator(); it.hasNext(); sb = sb + (String)item.getKey() + ":" + item.getValue() + ",") {
            item = (Entry)it.next();
        }

        return sb.toString();
    }

    public static void prepareDateParam(Map paramMap) {
        Date date = new Date();
        String DATE_WITHOUT_SEPERATOR = DateFormatUtils.format(date, "yyyyMMdd");
        String DATE_WITH_SEPERATOR = DateFormatUtils.format(date, "yyyy_MM_dd");
        String DATE_WITH_MID_SEPERATOR = DateFormatUtils.format(date, "yyyy-MM-dd");
        paramMap.put("DATE_WITH_MID_SEPERATOR", DATE_WITH_MID_SEPERATOR);
        paramMap.put("DATE_WITHOUT_SEPERATOR", DATE_WITHOUT_SEPERATOR);
        paramMap.put("DATE_WITH_SEPERATOR", DATE_WITH_SEPERATOR);
    }

    public static void prepareTomorrowDateParam(Map paramMap) {
        Date date = DateUtil.tomorrow(new Date());
        String DATE_WITHOUT_SEPERATOR = DateFormatUtils.format(date, "yyyyMMdd");
        String DATE_WITH_SEPERATOR = DateFormatUtils.format(date, "yyyy_MM_dd");
        String DATE_WITH_MID_SEPERATOR = DateFormatUtils.format(date, "yyyy-MM-dd");
        paramMap.put("DATE_WITH_MID_SEPERATOR", DATE_WITH_MID_SEPERATOR);
        paramMap.put("DATE_WITHOUT_SEPERATOR", DATE_WITHOUT_SEPERATOR);
        paramMap.put("DATE_WITH_SEPERATOR", DATE_WITH_SEPERATOR);
    }
}
