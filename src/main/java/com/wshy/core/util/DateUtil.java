//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.wshy.core.util;

import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang3.time.DateFormatUtils;

public class DateUtil {
    public static final String DATE_FORMAT_RFC_1123_2_YEAR = "EEE, dd MMM yy HH:mm:ss z";
    public static final String DATE_FORMAT_RFC_1123_4_YEAR = "EEE, dd MMM yyyy HH:mm:ss z";
    public static final String DATE_FORMAT_RFC_1123_NO_TIMEZONE = "EEE, dd MMM yy HH:mm:ss";
    public static final String DATE_FORMAT_RFC_1123_VARIANT_1 = "EEE, MMM dd yy HH:mm:ss";
    public static final String DATE_FORMAT_RFC_1123_NO_SECOND = "EEE, dd MMM yy HH:mm z";
    public static final String DATE_FORMAT_RFC_1123_VARIANT_2 = "EEE dd MMM yyyy HH:mm:ss";
    public static final String DATE_FORMAT_RFC_1123_NO_WEEKDAY = "dd MMM yy HH:mm:ss z";
    public static final String DATE_FORMAT_RFC_1123_NO_WEEKDAY_SECOND = "dd MMM yy HH:mm z";
    public static final String DATE_FORMAT_RFC_1123_NO_WEEKDAY_4_YEAR = "dd MMM yyyy HH:mm:ss z";
    public static final String DATE_FORMAT_RFC_1123_NO_WEEKDAY_SECOND_4_YEAR = "dd MMM yyyy HH:mm z";
    public static final String DATE_FORMAT_ISO_8601_MODE_1 = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static final String DATE_FORMAT_ISO_8601_MODE_2 = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String DATE_FORMAT_ISO_8601_MODE_3 = "yyyy-MM-dd'T'HH:mm:sszzzz";
    public static final String DATE_FORMAT_ISO_8601_MODE_4 = "yyyy-MM-dd'T'HH:mm:ss z";
    public static final String DATE_FORMAT_ISO_8601 = "yyyy-MM-dd'T'HH:mm:ssz";
    public static final String DATE_FORMAT_ISO_8601_MODE_5 = "yyyy-MM-dd'T'HH:mm:ss.SSSz";
    public static final String DATE_FORMAT_ISO_8601_MODE_6 = "yyyy-MM-dd'T'HHmmss.SSSz";
    public static final String DATE_FORMAT_ISO_8601_MODE_7 = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DATE_FORMAT_ISO_8601_MODE_8 = "yyyy-MM-dd'T'HH:mmZ";
    public static final String DATE_FORMAT_ISO_8601_MODE9 = "yyyy-MM-dd'T'HH:mm'Z'";
    public static final String DATE_FORMAT_SIMPLE = "yyyy-MM-dd";
    public static final String DATE_FORMAT_SIMPLE_2 = "MMM dd, yyyy";
    public static final String DATE_FORMAT_RFC_822_4_YEAR = "EEE, dd MMM yyyy HH:mm:ss Z";
    public static final String DATE_FORMAT_RFC_822_2_YEAR = "EEE, dd MMM yy HH:mm:ss Z";
    public static final String YEAR_MONTH = "yyyy-MM";
    public static final String YEAR_MONTH_WITHOUT_SEPERATOR = "yyyyMM";
    public static final String DATE = "yyyy-MM-dd";
    public static final String DATE_WITHOUT_SEPERATOR = "yyyyMMdd";
    public static final String DATE_HOUR = "yyyyMMddHH";
    public static final String DATE_SPACE_HOUR = "yyyy-MM-dd HH";
    public static final String DATE_SPACE_MINUTE = "yyyy-MM-dd HH:mm";
    public static final String TIMESTAMP = "yyyy-MM-dd HH:mm:ss";

    public DateUtil() {
    }

    public static String currentHour() {
        return DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMddHH");
    }

    public static String today() {
        return DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMdd");
    }

    public static String now() {
        return DateFormatUtils.format(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");
    }

    public static String currentMonth() {
        return DateFormatUtils.format(System.currentTimeMillis(), "yyyyMM");
    }

    public static long getTodayStart() {
        Calendar cal = Calendar.getInstance();
        cal.set(11, 0);
        cal.set(12, 0);
        cal.set(13, 0);
        return cal.getTime().getTime();
    }

    public static long getTodayEnd() {
        Calendar cal = Calendar.getInstance();
        cal.set(11, 23);
        cal.set(12, 59);
        cal.set(13, 59);
        return cal.getTime().getTime();
    }

    public static long getCurrentMonthFirstDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(5, 1);
        return calendar.getTime().getTime();
    }

    public static long getCurrentMonday() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(7, 2);
        return calendar.getTime().getTime();
    }

    public static Date yesterday(Date today) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(5, calendar.get(5) - 1);
        return calendar.getTime();
    }

    public static Date tomorrow(Date today) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(5, calendar.get(5) + 1);
        return calendar.getTime();
    }
}
