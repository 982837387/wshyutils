package com.wshy.core.autoconfig;

import com.wshy.core.exception.ExceptionCenter;
import com.wshy.core.helloword.HelloProperties;
import com.wshy.core.helloword.HelloService;
import com.wshy.core.sqlservice.FirstSqlService;
import com.wshy.core.util.DateUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wshy
 * @data 2021/2/9
 **/
@Configuration
//开启HelloProperties.class的自动属性绑定
//@EnableConfigurationProperties(HelloProperties.class)
public class HelloServiceAutoConfiguration {
    @Bean
    //只有容器中没有HelloService的时候才会注册该组件
    @ConditionalOnMissingBean(HelloService.class)
    public HelloService helloService() {
        return new HelloService();
    }

    @Bean
    //只有容器中没有HelloService的时候才会注册该组件
    @ConditionalOnMissingBean(FirstSqlService.class)
    public FirstSqlService FirstSqlService() {
        return new FirstSqlService();
    }

    @Bean
    //只有容器中没有HelloService的时候才会注册该组件
    @ConditionalOnMissingBean(ExceptionCenter.class)
    public ExceptionCenter ExceptionCenter() {
        return new ExceptionCenter();
    }

    @Bean
    @ConditionalOnMissingBean(DateUtil.class)
    public DateUtil DateUtil() {
        return new DateUtil();
    }

}

