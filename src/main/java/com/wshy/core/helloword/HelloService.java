package com.wshy.core.helloword;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author wshy
 * @data 2021/2/9
 **/
public class HelloService {
    //@Autowired
    //private HelloProperties helloProperties;
    @Value ("${hello.name}")
    private String name;

    @Value ("${hello.value}")
    private String value;

    public String sayHello(String str) {
        return name + str + value;
    }
}

