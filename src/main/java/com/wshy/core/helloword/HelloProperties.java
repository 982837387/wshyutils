package com.wshy.core.helloword;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author wshy
 * @data 2021/2/9
 **/
//@ConfigurationProperties(prefix = "hello")
public class HelloProperties {
    private String name;
    private String value;

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getValue () {
        return value;
    }

    public void setValue (String value) {
        this.value = value;
    }
}

