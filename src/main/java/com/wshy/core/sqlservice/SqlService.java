//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.wshy.core.sqlservice;

import com.alibaba.druid.sql.SQLUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wshy.core.exception.ExceptionCenter;
import com.wshy.core.util.ParamUtil;
import com.wshy.core.util.StringUtil;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class SqlService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Value("${spring.datasource.driverClassName:mysql}")
    private String driverClassName;
    @Value("${file.url}")
    private String fileUrl;
    @Value("${file.uploadDir}")
    private String filePath;

    public SqlService() {
    }

    private Map arrayToMap(Map param, Object... args) {
        if (param == null) {
            param = new HashMap();
        }

        for(int i = 0; i < args.length; i += 2) {
            ((Map)param).put(args[i], args[i + 1]);
        }

        return (Map)param;
    }

    private String concatSqlid(String namespace, String sqlid) {
        return StringUtil.isNullOrEmpty(namespace) ? sqlid : namespace + "." + sqlid;
    }

    public int update(SqlSession sqlSession, String namespace, String sqlid) {
        this.logger.debug("sqlid:" + namespace + "." + sqlid);
        String sql = this.getSql(sqlSession, namespace, sqlid, new HashMap());
        this.logger.debug("sql:" + sql);
        int ret = sqlSession.update(namespace + "." + sqlid);
        this.logger.debug("ret:" + ret);
        return ret;
    }

    public int update(SqlSession sqlSession, String namespace, String sqlid, Object... args) {
        this.logger.debug("sqlid:" + namespace + "." + sqlid);
        String sql = this.getSql(sqlSession, namespace, sqlid, this.arrayToMap((Map)null, args));
        this.logger.debug("sql:" + sql);
        int ret = sqlSession.update(namespace + "." + sqlid, this.arrayToMap((Map)null, args));
        this.logger.debug("ret:" + ret);
        return ret;
    }

    public int update(SqlSession sqlSession, String namespace, String sqlid, Map param, Object... args) {
        this.logger.debug("sqlid:" + namespace + "." + sqlid);
        String sql = this.getSql(sqlSession, namespace, sqlid, this.arrayToMap(param, args));
        this.logger.debug("sql:" + sql);
        int ret = sqlSession.update(namespace + "." + sqlid, this.arrayToMap(param, args));
        this.logger.debug("ret:" + ret);
        return ret;
    }

    public List selectList(SqlSession sqlSession, String namespace, String sqlid) {
        this.logger.debug("sqlid:" + namespace + "." + sqlid);
        String sql = this.getSql(sqlSession, namespace, sqlid, new HashMap());
        this.logger.debug("sql:" + sql);
        List<Map> list = sqlSession.selectList(namespace + "." + sqlid);
        this.logger.debug("list size:" + list.size());
        this.logger.debug("list:" + list);
        return list;
    }

    public List selectList(SqlSession sqlSession, String namespace, String sqlid, Object... args) {
        this.logger.debug("sqlid:" + namespace + "." + sqlid);
        String sql = this.getSql(sqlSession, namespace, sqlid, this.arrayToMap((Map)null, args));
        this.logger.debug("sql:" + sql);
        List<Map> list = sqlSession.selectList(namespace + "." + sqlid, this.arrayToMap((Map)null, args));
        this.logger.debug("list size:" + list.size());
        this.logger.debug("list:" + list);
        return list;
    }

    public List selectList(SqlSession sqlSession, String namespace, String sqlid, Map param, Object... args) {
        this.logger.debug("sqlid:" + namespace + "." + sqlid);
        String sql = this.getSql(sqlSession, namespace, sqlid, this.arrayToMap(param, args));
        this.logger.debug("sql:" + sql);
        List<Map> list = sqlSession.selectList(namespace + "." + sqlid, this.arrayToMap(param, args));
        this.logger.debug("list size:" + list.size());
        this.logger.debug("list:" + list);
        return list;
    }

    public Object selectOne(SqlSession sqlSession, String namespace, String sqlid) {
        this.logger.debug("sqlid:" + namespace + "." + sqlid);
        String sql = this.getSql(sqlSession, namespace, sqlid, new HashMap());
        this.logger.debug("sql:" + sql);
        Object obj = sqlSession.selectOne(namespace + "." + sqlid);
        this.logger.debug("obj:" + obj);
        return obj;
    }

    public Object selectOne(SqlSession sqlSession, String namespace, String sqlid, Object... args) {
        this.logger.debug("sqlid:" + namespace + "." + sqlid);
        String sql = this.getSql(sqlSession, namespace, sqlid, this.arrayToMap((Map)null, args));
        this.logger.debug("sql:" + sql);
        Object obj = sqlSession.selectOne(namespace + "." + sqlid, this.arrayToMap((Map)null, args));
        this.logger.debug("obj:" + obj);
        return obj;
    }

    public Object selectOne(SqlSession sqlSession, String namespace, String sqlid, Map param, Object... args) {
        this.logger.debug("sqlid:" + namespace + "." + sqlid);
        String sql = this.getSql(sqlSession, namespace, sqlid, this.arrayToMap(param, args));
        this.logger.debug("sql:" + sql);
        Object obj = sqlSession.selectOne(namespace + "." + sqlid, this.arrayToMap(param, args));
        this.logger.debug("obj:" + obj);
        return obj;
    }

    public Object antdTable(SqlSession sqlSession, String namespace, String sqlid, Map paramMap, Object... args) throws ExceptionCenter {
        paramMap = this.arrayToMap(paramMap, args);
        this.prepareParam(paramMap, this.driverClassName, "fileUrl", this.fileUrl, "filePath", this.filePath);
        Map retMap = new HashMap();
        String retKey = (String)paramMap.get("retKey");
        if (retKey != null) {
            retMap.put(paramMap.get("retKey"), this.selectOne(sqlSession, namespace, sqlid, paramMap));
            return retMap;
        } else {
            SqlCommandType sqlct = this.getSqlType(sqlSession, namespace, sqlid);
            String sql = this.getSql(sqlSession, namespace, sqlid, paramMap);
            this.logger.debug("sql:" + sql);
            List retList = null;
            if (SqlCommandType.SELECT == sqlct) {
                this.setSorter(paramMap);
                retList = (List)this.setPagination(sqlSession, paramMap, namespace, sqlid, retMap);
            } else {
                //int ret = false;
                int ret = sqlSession.update(namespace + "." + sqlid, paramMap);
                if (ret <= 0) {
                    throw new ExceptionCenter(HttpStatus.INTERNAL_SERVER_ERROR, "操作失败");
                }

                retMap.put("success", true);
                String returnSelect = (String)paramMap.get("returnSelect");
                if (StringUtil.isNullOrEmpty(returnSelect) || returnSelect.indexOf(".") <= 0) {
                    return retMap;
                }

                String[] returnArr = returnSelect.split("[.]");
                String returnNamespace = returnArr[0];
                String returnSqlid = returnArr[1];
                Map param = new HashMap();
                if (paramMap.get("current") != null) {
                    param.put("currentPage", paramMap.get("current"));
                }

                if (paramMap.get("pageSize") != null) {
                    param.put("pageSize", paramMap.get("pageSize"));
                }

                if (paramMap.get("closePagination") != null) {
                    param.put("closePagination", paramMap.get("closePagination"));
                }

                if (paramMap.get("user_merchantid") != null) {
                    param.put("user_merchantid", paramMap.get("user_merchantid"));
                }

                retList = (List)this.setPagination(sqlSession, param, returnNamespace, returnSqlid, retMap);
            }

            if (paramMap.get("listKey") == null) {
                retMap.put("list", retList);
            } else {
                Map<String, Object> data = new HashMap();
                data.put("list", retList);
                data.put("pagination", retMap.get("pagination"));
                retMap.remove("pagination");
                retMap.put(paramMap.get("listKey"), data);
            }

            return retMap;
        }
    }

    public Object excuteSql(SqlSession sqlSession, String namespace, String sqlid, Map paramMap, Object... args) throws ExceptionCenter {
        return this.antdTable(sqlSession, namespace, sqlid, paramMap, args);
    }

    public Object excuteSqlCheck(SqlSession sqlSession, String namespace, String sqlid, int size, String exception, Map paramMap, Object... args) throws ExceptionCenter {
        this.logger.debug("sqlid:" + namespace + "." + sqlid);
        paramMap = this.arrayToMap(paramMap, args);
        SqlCommandType sqlct = this.getSqlType(sqlSession, namespace, sqlid);
        String sql = this.getSql(sqlSession, namespace, sqlid, paramMap);
        this.logger.debug("sql:" + sql);
        if (SqlCommandType.SELECT == sqlct) {
            List<Map> list = sqlSession.selectList(namespace + "." + sqlid, paramMap);
            this.logger.debug("obj: size:" + list.size() + "," + list);
            if (list.size() != size && !StringUtil.isNullOrEmpty(exception)) {
                throw new ExceptionCenter(HttpStatus.INTERNAL_SERVER_ERROR, exception);
            } else {
                return list;
            }
        } else {
            //int ret = false;
            int ret = sqlSession.update(namespace + "." + sqlid, paramMap);
            this.logger.debug("obj: ret:" + ret);
            if (ret != size && !StringUtil.isNullOrEmpty(exception)) {
                throw new ExceptionCenter(HttpStatus.INTERNAL_SERVER_ERROR, exception);
            } else {
                return ret;
            }
        }
    }

    public Object excuteSqlCheck(SqlSession sqlSession, String namespace, String sqlid, int size, String returnCode, String exception, Map paramMap, Object... args) throws ExceptionCenter {
        this.logger.debug("sqlid:" + namespace + "." + sqlid);
        paramMap = this.arrayToMap(paramMap, args);
        SqlCommandType sqlct = this.getSqlType(sqlSession, namespace, sqlid);
        String sql = this.getSql(sqlSession, namespace, sqlid, paramMap);
        this.logger.debug("sql:" + sql);
        if (SqlCommandType.SELECT == sqlct) {
            List<Map> list = sqlSession.selectList(namespace + "." + sqlid, paramMap);
            this.logger.debug("obj: size:" + list.size() + "," + list);
            if (list.size() != size && !StringUtil.isNullOrEmpty(exception)) {
                throw new ExceptionCenter(returnCode, exception);
            } else {
                return list;
            }
        } else {
            //int ret = false;
            int ret = sqlSession.update(namespace + "." + sqlid, paramMap);
            this.logger.debug("obj: ret:" + ret);
            if (ret != size && !StringUtil.isNullOrEmpty(exception)) {
                throw new ExceptionCenter(returnCode, exception);
            } else {
                return ret;
            }
        }
    }

    public Object process(SqlSession sqlSession, String namespace, String sqlid, Object... args) throws ExceptionCenter {
        return this.excuteSql(sqlSession, namespace, sqlid, new HashMap(), args);
    }

    public Object process(SqlSession sqlSession, String namespace, String sqlid, Map param, Object... args) throws ExceptionCenter {
        return this.excuteSql(sqlSession, namespace, sqlid, param, args);
    }

    public Object process(SqlSession sqlSession, String namespace, String sqlid, int size, String exception, Map param, Object... args) throws ExceptionCenter {
        return this.excuteSqlCheck(sqlSession, namespace, sqlid, size, exception, param, args);
    }

    private void setSorter(Map paramMap) {
        String sorter = (String)paramMap.get("sorter");
        if (!StringUtil.isNullOrEmpty(sorter)) {
            int index = sorter.lastIndexOf("_");
            String field = sorter.substring(0, index);
            paramMap.put("sort_key", field);
            paramMap.put("sort_way", "descend".equals(sorter.substring(index + 1)) ? "desc" : "asc");
        }

    }

    private Object setPagination(SqlSession sqlSession, Map paramMap, String namespace, String sqlid, Map retMap) throws ExceptionCenter {
        Map pagination = new HashMap();
        List retList = null;
        //int currentPage = false;
        //int pageSize = false;
        PageInfo pageInfo;
        long totalPage;
        if (paramMap.get("currentPage") != null && paramMap.get("pageSize") != null) {
            int currentPage = (Integer)paramMap.get("currentPage");
            int pageSize = (Integer)paramMap.get("pageSize");
            PageHelper.startPage(currentPage, pageSize);
            retList = sqlSession.selectList(namespace + "." + sqlid, paramMap);
            pageInfo = new PageInfo(retList);
            if (pageInfo.getSize() == 0 && currentPage > 1) {
                --currentPage;
                PageHelper.startPage(currentPage, pageSize);
                retList = sqlSession.selectList(namespace + "." + sqlid, paramMap);
                pageInfo = new PageInfo(retList);
            }

            pagination.put("current", currentPage);
            pagination.put("pageSize", pageSize);
            pagination.put("total", pageInfo.getTotal());
            totalPage = 0L;
            totalPage = pageInfo.getTotal() / (long)pageSize;
            if (pageInfo.getTotal() % (long)pageSize != 0L) {
                ++totalPage;
            }

            pagination.put("totalPage", totalPage);
        } else if (paramMap.get("closePagination") == null) {
            PageHelper.startPage(1, 10);
            retList = sqlSession.selectList(namespace + "." + sqlid, paramMap);
            pageInfo = new PageInfo(retList);
            pagination.put("current", 1);
            pagination.put("pageSize", 10);
            pagination.put("total", pageInfo.getTotal());
            totalPage = 0L;
            totalPage = pageInfo.getTotal() / 10L;
            if (pageInfo.getTotal() % 10L != 0L) {
                ++totalPage;
            }

            pagination.put("totalPage", totalPage);
        } else {
            retList = sqlSession.selectList(namespace + "." + sqlid, paramMap);
        }

        if (paramMap.get("needsum") != null) {
            paramMap.put("total", 1);
            List totalList = sqlSession.selectList(namespace + "." + sqlid, paramMap);
            if (totalList.size() > 0) {
                if ((Map)totalList.get(0) == null) {
                    throw new ExceptionCenter(HttpStatus.INTERNAL_SERVER_ERROR, "暂无数据");
                }

                pagination.put("total", ((Map)totalList.get(0)).get("total"));
                pagination.put("sum", ((Map)totalList.get(0)).get("sum"));
            } else {
                pagination.put("total", 0);
            }
        }

        if (retList.size() == 0) {
            pagination.put("total", 0);
        }

        retMap.put("pagination", pagination);
        return retList;
    }

    public SqlCommandType getSqlCommandType(SqlSession sqlSession, String namespace, String sqlid) {
        MappedStatement ms = sqlSession.getConfiguration().getMappedStatement(namespace + "." + sqlid);
        SqlCommandType sqlct = ms.getSqlCommandType();
        return sqlct;
    }

    public void prepareParam(Map<String, Object> paramMap, String driverClassName, String... args) throws ExceptionCenter {
        ParamUtil.filterEmpty(paramMap);
        ParamUtil.addDataSource(paramMap, driverClassName);
        this.arrayToMap(paramMap, args);
    }

    public SqlCommandType getSqlType(SqlSession sqlSession, String namespace, String sqlid) {
        MappedStatement ms = sqlSession.getConfiguration().getMappedStatement(namespace + "." + sqlid);
        SqlCommandType sqlct = ms.getSqlCommandType();
        return sqlct;
    }

    public String getSql(SqlSession sqlSession, String namespace, String sqlid, Map paramMap) {
        MappedStatement ms = sqlSession.getConfiguration().getMappedStatement(namespace + "." + sqlid);
        String sqlId = ms.getId();
        BoundSql boundSql = ms.getBoundSql(paramMap);
        Configuration configuration = ms.getConfiguration();
        String sql = this.getSql(configuration, boundSql, sqlId);
        String formatSql = SQLUtils.formatMySql(sql.substring(sql.indexOf(":") + 1));
        return formatSql;
    }

    private String getSql(Configuration configuration, BoundSql boundSql, String sqlId) {
        String sql = this.showSql(configuration, boundSql);
        StringBuilder str = new StringBuilder(100);
        str.append(sqlId);
        str.append(":");
        str.append(sql);
        return str.toString();
    }

    private static String getParameterValue(Object obj) {
        String value = null;
        if (obj instanceof String) {
            value = "'" + obj.toString() + "'";
        } else if (obj instanceof Date) {
            DateFormat formatter = DateFormat.getDateTimeInstance(2, 2, Locale.CHINA);
            value = "'" + formatter.format(new Date()) + "'";
        } else if (obj != null) {
            value = obj.toString();
        } else {
            value = "";
        }

        return value;
    }

    private String showSql(Configuration configuration, BoundSql boundSql) {
        Object parameterObject = boundSql.getParameterObject();
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
        String sql = boundSql.getSql().replaceAll("[\\s]+", " ");
        if (CollectionUtils.isNotEmpty(parameterMappings) && parameterObject != null) {
            TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
            if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
                sql = sql.replaceFirst("\\?", Matcher.quoteReplacement(getParameterValue(parameterObject)));
            } else {
                MetaObject metaObject = configuration.newMetaObject(parameterObject);
                Iterator var8 = parameterMappings.iterator();

                while(var8.hasNext()) {
                    ParameterMapping parameterMapping = (ParameterMapping)var8.next();
                    String propertyName = parameterMapping.getProperty();
                    Object obj;
                    if (metaObject.hasGetter(propertyName)) {
                        obj = metaObject.getValue(propertyName);
                        sql = sql.replaceFirst("\\?", Matcher.quoteReplacement(getParameterValue(obj)));
                    } else if (boundSql.hasAdditionalParameter(propertyName)) {
                        obj = boundSql.getAdditionalParameter(propertyName);
                        sql = sql.replaceFirst("\\?", Matcher.quoteReplacement(getParameterValue(obj)));
                    } else {
                        sql = sql.replaceFirst("\\?", "缺失");
                    }
                }
            }
        }

        return sql;
    }
}
