//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.wshy.core.sqlservice;

import com.wshy.core.exception.ExceptionCenter;
import com.wshy.core.util.ParamUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class FirstSqlService implements SqlServiceInf {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private SqlSession sqlSession;
    @Autowired
    private SqlService sqlService;

    public FirstSqlService () {
    }

    @Override
    public int update(String namespace, String sqlid) {
        return this.sqlService.update(this.sqlSession, namespace, sqlid);
    }

    @Override
    public int update(String namespace, String sqlid, Object... args) {
        return this.sqlService.update(this.sqlSession, namespace, sqlid, args);
    }

    @Override
    public int update(String namespace, String sqlid, Map param, Object... args) {
        return this.sqlService.update(this.sqlSession, namespace, sqlid, param, args);
    }

    @Override
    public List selectList(String namespace, String sqlid) {
        return this.sqlService.selectList(this.sqlSession, namespace, sqlid);
    }

    @Override
    public List selectList(String namespace, String sqlid, Object... args) {
        return this.sqlService.selectList(this.sqlSession, namespace, sqlid, args);
    }

    @Override
    public List selectList(String namespace, String sqlid, Map param, Object... args) {
        return this.sqlService.selectList(this.sqlSession, namespace, sqlid, param, args);
    }

    @Override
    public Object selectOne(String namespace, String sqlid) {
        return this.sqlService.selectOne(this.sqlSession, namespace, sqlid);
    }

    @Override
    public Object selectOne(String namespace, String sqlid, Object... args) {
        return this.sqlService.selectOne(this.sqlSession, namespace, sqlid, args);
    }

    @Override
    public Object selectOne(String namespace, String sqlid, Map param, Object... args) {
        return this.sqlService.selectOne(this.sqlSession, namespace, sqlid, param, args);
    }

    @Override
    public Object excuteSql(String namespace, String sqlid) throws ExceptionCenter {
        return this.sqlService.excuteSql(this.sqlSession, namespace, sqlid, (Map)null, new Object[0]);
    }

    @Override
    public Object excuteSql(String namespace, String sqlid, Object... args) throws ExceptionCenter {
        return this.sqlService.excuteSql(this.sqlSession, namespace, sqlid, (Map)null, args);
    }

    @Override
    public Object excuteSql(String namespace, String sqlid, Map paramMap, Object... args) throws ExceptionCenter {
        return this.sqlService.excuteSql(this.sqlSession, namespace, sqlid, paramMap, args);
    }

    @Override
    public Object excuteSqlCheck(String namespace, String sqlid, int size, String exception, Map paramMap, Object... args) throws ExceptionCenter {
        return this.sqlService.excuteSqlCheck(this.sqlSession, namespace, sqlid, size, exception, paramMap, args);
    }

    @Override
    public Object excuteSqlCheck(String namespace, String sqlid, int size, String returnCode, String exception, Map paramMap, Object... args) throws ExceptionCenter {
        return this.sqlService.excuteSqlCheck(this.sqlSession, namespace, sqlid, size, returnCode, exception, paramMap, args);
    }

    @Override
    public Object process(String namespace, String sqlid) throws ExceptionCenter {
        return this.sqlService.process(this.sqlSession, namespace, sqlid, new Object[0]);
    }

    @Override
    public Object process(String namespace, String sqlid, Object... args) throws ExceptionCenter {
        return this.sqlService.process(this.sqlSession, namespace, sqlid, args);
    }

    @Override
    public Object process(String namespace, String sqlid, Map param, Object... args) throws ExceptionCenter {
        return this.sqlService.process(this.sqlSession, namespace, sqlid, param, args);
    }

    public Object kv(String namespace, String sqlid, Map paramMap) throws ExceptionCenter {
        Map retMap = new HashMap();
        new HashMap();
        String tradeCode = ParamUtil.getMapValue(paramMap, "tradeCode", "");
        String var7 = ParamUtil.getMapValue(paramMap, "tradeCodeList", "");

        try {
            this.logger.debug("" + paramMap);
            List resultList;
            if (namespace != null && sqlid != null) {
                resultList = this.selectList(namespace, sqlid, paramMap);
            } else {
                resultList = this.selectList("common", "selectKeyValue", paramMap);
            }

            Map kv = new HashMap();
            List<Map> tvList = new ArrayList();
            List<Map> lvList = new ArrayList();

            HashMap lv;
            for(Iterator var12 = resultList.iterator(); var12.hasNext(); lvList.add(lv)) {
                Map map = (Map)var12.next();
                kv.put(map.get(paramMap.get("key")), map.get(paramMap.get("value")));
                Map tv = new HashMap();
                tv.put("text", map.get(paramMap.get("value")));
                if (paramMap.get("needStr") != null) {
                    tv.put("value", "" + map.get(paramMap.get("key")));
                } else {
                    tv.put("value", map.get(paramMap.get("key")));
                }

                tvList.add(tv);
                lv = new HashMap();
                lv.put("label", map.get(paramMap.get("value")));
                if (paramMap.get("needStr") != null) {
                    lv.put("value", "" + map.get(paramMap.get("key")));
                } else {
                    lv.put("value", map.get(paramMap.get("key")));
                }
            }

            Map table = new HashMap();
            table.put("kv", kv);
            table.put("tv", tvList);
            table.put("lv", lvList);
            String table_cat = (String)paramMap.get("retKey") + (paramMap.get("catid") == null ? "" : "_" + paramMap.get("catid"));
            retMap.put(table_cat, table);
        } catch (Exception var16) {
            var16.printStackTrace();
            this.logger.debug("selectKV:" + var16.toString());
            if (var16.getMessage() != null) {
                this.logger.debug("message:" + var16.getMessage());
            }

            if (var16.getCause() != null) {
                this.logger.debug("cause:" + var16.getCause());
            }

            throw new ExceptionCenter(HttpStatus.INTERNAL_SERVER_ERROR, ParamUtil.getError(var16));
        }

        this.logger.debug("retMap:" + retMap.toString());
        return retMap;
    }
}
