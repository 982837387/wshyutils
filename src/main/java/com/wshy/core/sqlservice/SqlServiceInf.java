package com.wshy.core.sqlservice;

import com.wshy.core.exception.ExceptionCenter;

import java.util.List;
import java.util.Map;

/**
 * @author wshy
 * @data 2021/2/9
 **/
public interface SqlServiceInf {
    int update(String namespace, String sqlid);

    int update(String namespace, String sqlid, Object... args);

    int update(String namespace, String sqlid, Map param, Object... args);

    List selectList(String namespace, String sqlid);

    List selectList(String namespace, String sqlid, Object... args);

    List selectList(String namespace, String sqlid, Map param, Object... args);

    Object selectOne(String namespace, String sqlid);

    Object selectOne(String namespace, String sqlid, Object... args);

    Object selectOne(String namespace, String sqlid, Map param, Object... args);

    Object excuteSql(String namespace, String sqlid) throws ExceptionCenter;

    Object excuteSql(String namespace, String sqlid, Object... args) throws ExceptionCenter;

    Object excuteSql(String namespace, String sqlid, Map paramMap, Object... args) throws ExceptionCenter;

    Object excuteSqlCheck(String namespace, String sqlid, int size, String exception, Map paramMap, Object... args) throws ExceptionCenter;

    Object excuteSqlCheck(String namespace, String sqlid, int size, String returnCode, String exception, Map paramMap, Object... args) throws ExceptionCenter;

    Object process(String namespace, String sqlid) throws ExceptionCenter;

    Object process(String namespace, String sqlid, Object... args) throws ExceptionCenter;

    Object process(String namespace, String sqlid, Map param, Object... args) throws ExceptionCenter;
}
