//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.wshy.core.exception;

import java.util.Date;
import org.apache.commons.lang3.time.DateFormatUtils;

public class ErrorDetails {
    private String timestamp = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
    private boolean success;
    private String returnMsg;
    private String returnCode;

    public String getReturnMsg() {
        return this.returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public String getReturnCode() {
        return this.returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public ErrorDetails(String returnCode, String details) {
        this.returnMsg = details;
        this.returnCode = returnCode;
        this.success = false;
    }

    public String getTimestamp() {
        return this.timestamp;
    }
}
