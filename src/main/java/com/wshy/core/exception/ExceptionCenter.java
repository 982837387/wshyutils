//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.wshy.core.exception;

import java.util.Date;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class ExceptionCenter extends Exception {
    private HttpStatus status;
    private String timestamp;
    private String message;
    private String details;
    private boolean success;
    private String returnCode;
    private String tablename;
    private String originrecord;
    private String param;
    private String exception;
    private String tradeCode;

    public String getReturnCode() {
        return this.returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getTablename() {
        return this.tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public String getOriginrecord() {
        return this.originrecord;
    }

    public void setOriginrecord(String originrecord) {
        this.originrecord = originrecord;
    }

    public String getParam() {
        return this.param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getException() {
        return this.exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getTradeCode() {
        return this.tradeCode;
    }

    public void setTradeCode(String tradeCode) {
        this.tradeCode = tradeCode;
    }

    @Override
    public String toString() {
        return "{" + this.status + "," + this.timestamp + "," + this.message + "," + this.details + "}";
    }

    public ExceptionCenter() {
        this.timestamp = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        this.success = false;
    }

    public ExceptionCenter(HttpStatus status) {
        this.status = status;
        this.timestamp = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        this.success = false;
    }

    public ExceptionCenter(String returnCode) {
        this.timestamp = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        this.success = false;
        this.returnCode = returnCode;
    }

    public ExceptionCenter(String returnCode, String details) {
        this.timestamp = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        this.success = false;
        this.returnCode = returnCode;
        this.details = details;
        this.message = details;
    }

    public ExceptionCenter(HttpStatus status, String details) {
        this.status = status;
        this.details = details;
        this.message = details;
        this.timestamp = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
    }

    public ExceptionCenter(HttpStatus status, String details, String tablename, String originrecord, String param, String tradeCode, String exception) {
        this.status = status;
        this.details = details;
        this.timestamp = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        this.success = false;
        this.tablename = tablename;
        this.originrecord = originrecord;
        this.param = param;
        this.tradeCode = tradeCode;
        this.exception = exception;
    }

    public ExceptionCenter(HttpStatus status, String message, String details) {
        this.status = status;
        this.message = message;
        this.details = details;
        this.timestamp = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        this.success = false;
    }

    public HttpStatus getStatus() {
        return this.status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return this.details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
