//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.wshy.core.exception;

import com.wshy.core.sqlservice.FirstSqlService;
import com.wshy.core.util.ParamUtil;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CustomizedResponseEntityExceptionHandler {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private FirstSqlService firstSqlService;

    public CustomizedResponseEntityExceptionHandler() {
    }

    @ExceptionHandler({ExceptionCenter.class})
    public final ResponseEntity<ErrorDetails> handleExceptionCenters(ExceptionCenter e) {
        this.logger.debug("ExceptionCenter:" + e.toString());
        this.logger.debug("ExceptionCenter:" + e.getDetails());
        this.logger.debug("ExceptionCenter:" + e.getMessage());
        ErrorDetails ed = null;
        this.logger.debug("e.getReturnCode():" + e.getReturnCode());
        this.logger.debug("e.getStatus():" + e.getStatus());
        if (e.getStatus() != null) {
            ed = new ErrorDetails(String.valueOf(e.getStatus().value()), e.getDetails());
            return new ResponseEntity(ed, e.getStatus());
        } else {
            if (e.getReturnCode() != null) {
                ed = new ErrorDetails(e.getReturnCode(), e.getDetails());
            } else if (e.getDetails() != null) {
                ed = new ErrorDetails(ParamUtil.getErrorCode(e), e.getDetails());
            } else {
                ed = new ErrorDetails(ParamUtil.getErrorCode(e), ParamUtil.getError(e));
            }

            return new ResponseEntity(ed, HttpStatus.OK);
        }
    }

    public void saveException(Map paramMap) {
        this.firstSqlService.update("exception", "insertSelective", paramMap, new Object[0]);
    }

    @ExceptionHandler({Exception.class})
    @ResponseBody
    public final ResponseEntity<Object> handleExceptions(Exception e) throws ExceptionCenter {
        this.logger.debug("Exception:" + e.toString());
        e.printStackTrace();
        ErrorDetails ed = new ErrorDetails(ParamUtil.getErrorCode(e), ParamUtil.getError(e));
        return new ResponseEntity(ed, HttpStatus.OK);
    }
}
